import { Injectable } from '@angular/core';
import { PingviniService } from '../services/pingvini.service';

@Injectable()
export class PingviniModel {

    pingvini = [];

    constructor(private service: PingviniService) {
        this.refreshujPingvine(()=>{});
    }

    refreshujPingvine(clbk) {
        this.service.getAll().subscribe(
            (pingvini) => {
                this.pingvini = pingvini;
                clbk()
            }
        );
    }

    updejtujPingvina(pingvin){
        this.service.update(pingvin).subscribe((updejtovaniPingvin)=>{
            this.refreshujPingvine(()=>{})
        })
    }

    getPingvinaById(id, clbk){
        this.service.getById(id).subscribe(clbk);
    }

    getPingvinaByIdCallback(id, clbk){
        if(this.pingvini.length < 1){
            this.refreshujPingvine(()=>{
                for (let i = 0; i < this.pingvini.length; i++) {
                    if (id == this.pingvini[i].id) {
                        clbk(this.pingvini[i])
                    }
                }
            })
        }else{
            for (let i = 0; i < this.pingvini.length; i++) {
                if (id == this.pingvini[i].id) {
                    clbk(this.pingvini[i])
                }
            }
        }
        
    }

    dodajPingvina(pingvin) {
        this.service.post(pingvin).subscribe(
            (pingvin) => {
                this.pingvini.push(pingvin);
            }
        )
    }

    dajMiIndexPingvinaPoIdju(id,clbk) {
        for (let i = 0; i < this.pingvini.length; i++) {
            if (id == this.pingvini[i].id) {
                clbk(i)
            }
        }
    }

    obrisiPingvina(id) {
        this.service.delete(id).subscribe(() => {
           this.dajMiIndexPingvinaPoIdju(id,(index)=>{
                this.pingvini.splice(index,1);
            });
           
        });
    }

}