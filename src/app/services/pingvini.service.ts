import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { config } from '../config';
import "rxjs/add/operator/map";

@Injectable()
export class PingviniService {

    constructor(private http: Http) { }

    getAll() {
        return this.http
            .get(config.apiUrl + '/pingvini')
            .map((response) => { return response.json() });
    }

    getById(id) {
        return this.http
            .get(config.apiUrl + '/pingvini/' + id)
            .map((response) => { return response.json() });
    }

    post(pingvin) {
        return this.http
            .post(config.apiUrl + '/pingvini', pingvin)
            .map((response) => { return response.json() });
    }

    update(pingvin) {
        return this.http
            .patch(config.apiUrl + '/pingvini/' + pingvin.id, pingvin)
            .map((response) => { return response.json() });
    }

    delete(id) {
        return this.http
            .delete(config.apiUrl + '/pingvini/' + id)
            .map((response) => { return response.json() });
    }



}