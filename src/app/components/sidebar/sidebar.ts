import { Component, OnInit } from '@angular/core';
import { MenuModel } from '../../models/menu.model';

@Component({
  selector: 'asidebar',
  templateUrl: './sidebar.html',
})
export class SidebarComponent implements OnInit {

  constructor(public model:MenuModel) { }

  ngOnInit() {
  }

}
