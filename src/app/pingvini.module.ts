import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { PingviniComponent } from './pingvini.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';

import { MenuModel } from './models/menu.model';

import { HomeComponent } from './routes/home/home.route';
import { TablePingvinaComponent } from './routes/table-pingvina/table-pingvina.route';
import { NoviPingvinComponent } from './routes/novi-pingvin/novi-pingvin.route';
import { PingvinComponent } from './routes/pingvin/pingvin.route';
import { EditujPingvinaComponent } from './routes/edituj-pingvina/edituj-pingvina.route';
import { DetaljiPingvinaComponent } from './routes/detalji-pingvina/detalji-pingvina.route';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';
import { PingviniModel } from './models/pingvini.model';
import { PingviniService } from './services/pingvini.service';
import { FilterPipe } from './pipes/filter.pipe';
import {HighlightDirective} from './directives/highlight'

import { MyDatePickerModule } from '../../node_modules/angular4-datepicker/src/my-date-picker';

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "dashboard", component: TablePingvinaComponent },
  { path: "pingvini/novi", component: NoviPingvinComponent },
  {
    path: "pingvin/:id", component: PingvinComponent, children: [
      { path: "edit", component: EditujPingvinaComponent },
      { path: "info", component: DetaljiPingvinaComponent },
    ]
  },
 
]

@NgModule({
  declarations: [
    PingviniComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    TablePingvinaComponent,
    NoviPingvinComponent,
    PingvinComponent,
    EditujPingvinaComponent,
    DetaljiPingvinaComponent,
    MenuLinkComponent,
    FilterPipe,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    MyDatePickerModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    MenuModel,
    PingviniModel,
    PingviniService
  ],
  bootstrap: [PingviniComponent]
})
export class PingviniModule { }
