import { Component, OnInit } from '@angular/core';
import { PingviniModel } from '../../models/pingvini.model';
import { config } from '../../config';
import { Router} from '@angular/router';

@Component({
  selector: 'table-pingvina',
  templateUrl: './table-pingvina.route.html'
})
export class TablePingvinaComponent implements OnInit {
  searchString = "";
  conf = config.pingviniTableConfig;

  constructor(public model:PingviniModel, private router:Router) { }

  ngOnInit() {
  }

  obrisiPingvina(id){
    this.model.obrisiPingvina(id);
  }

  editujPingvina(id){
    this.router.navigate(['/pingvin',id,'edit']);
  }

}
