import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PingviniModel } from '../../models/pingvini.model';


@Component({
  selector: 'edituj-pingvina',
  templateUrl: './edituj-pingvina.route.html'
})
export class EditujPingvinaComponent implements OnInit {

  editovaniPingvin = {};

  constructor(private route:ActivatedRoute,public model:PingviniModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getPingvinaById(id,(pingvin)=>{
          this.editovaniPingvin = pingvin;
        });
      }
    );
  }

  updejtujPingvina(){
    this.model.updejtujPingvina(this.editovaniPingvin);
  }

  ngOnInit() {
  }

}
