import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PingviniModel } from '../../models/pingvini.model';

@Component({
  selector: 'detalji-pingvina',
  templateUrl: './detalji-pingvina.route.html'
})
export class DetaljiPingvinaComponent implements OnInit {

  pingvin = {};
  constructor(private route:ActivatedRoute,public model:PingviniModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getPingvinaById(id,(pingvin)=>{
          this.pingvin = pingvin;
        });
      }
    );
  }

  ngOnInit() {

  }

}
