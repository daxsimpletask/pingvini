import { Component, OnInit } from '@angular/core';
import { PingviniModel } from '../../models/pingvini.model';

import {IMyDpOptions} from '../../../../node_modules/angular4-datepicker/src/my-date-picker'
@Component({
  selector: 'novi-pingvin',
  templateUrl: './novi-pingvin.route.html'
})
export class NoviPingvinComponent implements OnInit {

  noviPingvin = {
    ime:null,
    kilaza:null,
    birthdate:{
      date:{
        day:1,
        month:1,
        year:2018
      }
    }
  };

  mojiOptionsi:IMyDpOptions = {
    inline:true,
    openSelectorOnInputClick:true,
    dateFormat: 'dd.mm.yyyy',
};


  constructor(public model:PingviniModel) { }

  dodajPingvina(){
    // if(this.noviPingvin.ime && this.noviPingvin.kilaza){
    //   this.model.dodajPingvina(this.noviPingvin);
    // }
    console.log(this.noviPingvin)
    
  }

  ngOnInit() {
  }

}
